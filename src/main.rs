use color_thief::{Color, ColorFormat};
use image::io::Reader as ImageReader;
use image::Rgba;
use imageproc::drawing::{draw_filled_rect_mut, draw_text_mut};
use imageproc::rect::Rect;
use rusttype::{point, Font, Scale};
use serde_derive::{Deserialize, Serialize};
use serde_xml_rs::from_str;
use std::env;
use std::fs::File;
use std::io::{BufReader, Cursor, Read};
use std::path::PathBuf;
use std::process::Command;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct Image {
    url: String,
    copyright: String,
    headline: String,
    fullstartdate: u64,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct Images {
    image: Vec<Image>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let exe = env::current_exe()?;
    let basepath = exe.parent().expect("Executable must be in some directory");
    let mut color = rgb::RGB::new(255u8, 255u8, 255u8); //White color;

    const BASEURL: &str = "https://www.bing.com";

    let xmlurl = format!(
        "{}{}",
        BASEURL, "/HPImageArchive.aspx?format=xml&idx=0&n=1&mkt=en-US"
    );

    let body = reqwest::get(xmlurl).await?.text().await?;

    let item: Images = from_str(&body).unwrap();

    let headline = match item.image.first() {
        Some(t) => t.headline.clone(),
        _ => String::new(),
    };

    let mut copyright: String = match item.image.first() {
        Some(t) => t.copyright.clone(),
        _ => String::new(),
    };

    let mut subtitle: String = String::new();
    if !copyright.is_empty() {
        let c = copyright.as_str();
        if let Some(p) = c.rfind('(') {
            subtitle = String::from(c.get(..p).unwrap().trim());
            copyright = String::from(c.get(p + 1..copyright.len() - 1).unwrap().trim());
        }
    }

    let url = match item.image.first() {
        Some(u) => u.url.clone(),
        _ => String::new(),
    };

    let imgurl = format!("{}{}", BASEURL, url);

    let bytes = reqwest::get(imgurl).await?.bytes().await?;

    let reader = ImageReader::new(Cursor::new(bytes.clone()))
        .with_guessed_format()
        .expect("Cursor io never fails");

    let mut image = reader.decode()?;

    let reader = ImageReader::new(Cursor::new(bytes))
        .with_guessed_format()
        .expect("Cursor io never fails");
    let dim = reader.into_dimensions()?;

    if let Ok(colors) = get_colors(image.as_bytes(), image.color()) {
        if let Some(c) = colors.first() {
            color = *c;
        }
    }

    let tc = get_text_color(&color);

    let mut fontpath = basepath.join("font");
    fontpath.push("DejaVuSans.ttf");

    let f = File::open(fontpath)?;
    let mut reader = BufReader::new(f);
    let mut buffer = Vec::new();

    reader.read_to_end(&mut buffer)?;
    let font = Font::try_from_vec(buffer).expect("Failed to load font");

    let height = 22.0;
    let scale = Scale {
        x: height,
        y: height,
    };

    let v_metrics = font.v_metrics(scale);

    let mut y = 40;
    let padding_x: i32 = 5;
    let padding_y: i32 = 3;
    if !headline.is_empty() {
        let glyphs: Vec<_> = font.layout(&headline, scale, point(0.0, 0.0)).collect();

        let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;
        let glyphs_width = {
            let min_x = glyphs
                .first()
                .map(|g| g.pixel_bounding_box().unwrap().min.x)
                .unwrap();
            let max_x = glyphs
                .last()
                .map(|g| g.pixel_bounding_box().unwrap().max.x)
                .unwrap();
            (max_x - min_x) as u32
        };

        let x: i32 = (dim.0 - glyphs_width - 20) as i32;

        let rect = Rect::at(x - padding_x, y - padding_y).of_size(
            glyphs_width + (padding_x as u32 * 2),
            glyphs_height + (padding_y as u32 * 2),
        );
        draw_filled_rect_mut(&mut image, rect, Rgba([color.r, color.g, color.b, 127u8]));

        draw_text_mut(
            &mut image,
            Rgba([tc.r, tc.g, tc.b, 255u8]),
            x,
            y,
            scale,
            &font,
            &headline,
        );
    }

    if !subtitle.is_empty() {
        let glyphs: Vec<_> = font.layout(&subtitle, scale, point(0.0, 0.0)).collect();

        let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;
        let glyphs_width = {
            let min_x = glyphs
                .first()
                .map(|g| g.pixel_bounding_box().unwrap().min.x)
                .unwrap();
            let max_x = glyphs
                .last()
                .map(|g| g.pixel_bounding_box().unwrap().min.x)
                .unwrap();
            (max_x - min_x) as u32
        };

        y += 40;
        let x: i32 = (dim.0 - glyphs_width - 30) as i32;

        let rect = Rect::at(x - padding_x, y - padding_y).of_size(
            glyphs_width + (padding_x as u32 * 4), //Why this is a 4 and not a 2 I cannot answer
            glyphs_height + (padding_y as u32 * 2),
        );
        draw_filled_rect_mut(&mut image, rect, Rgba([color.r, color.g, color.b, 127u8]));

        draw_text_mut(
            &mut image,
            Rgba([tc.r, tc.g, tc.b, 255u8]),
            x,
            y,
            scale,
            &font,
            &subtitle,
        );
    }

    if !copyright.is_empty() {
        let glyphs: Vec<_> = font.layout(&copyright, scale, point(0.0, 0.0)).collect();

        let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;
        let glyphs_width = {
            let min_x = glyphs
                .first()
                .map(|g| g.pixel_bounding_box().unwrap().min.x)
                .unwrap();
            let max_x = glyphs
                .last()
                .map(|g| g.pixel_bounding_box().unwrap().max.x)
                .unwrap();
            (max_x - min_x) as u32
        };

        y += 40;
        let x: i32 = (dim.0 - glyphs_width - 20) as i32;

        let rect = Rect::at(x - padding_x, y - padding_y).of_size(
            glyphs_width + (padding_x as u32 * 2),
            glyphs_height + (padding_y as u32 * 2),
        );
        draw_filled_rect_mut(&mut image, rect, Rgba([color.r, color.g, color.b, 127u8]));

        draw_text_mut(
            &mut image,
            Rgba([tc.r, tc.g, tc.b, 255u8]),
            x,
            y,
            scale,
            &font,
            &copyright,
        );
    }

    let imgpath = basepath.join("image.jpg");
    let imgpath = get_os_img_path(imgpath);

    image.save(&imgpath)?;

    wallpaper::set_from_path(&imgpath)?;

    if cfg!(target_os = "macos") {
        let _ = Command::new("killall").arg("Dock").status();
    } else {
        wallpaper::set_mode(wallpaper::Mode::Stretch)?;
    }

    Ok(())
}

fn get_os_img_path(imgpath: PathBuf) -> String {
    imgpath.into_os_string().into_string().unwrap()
}

fn get_colors(img: &[u8], t: image::ColorType) -> Result<Vec<Color>, color_thief::Error> {
    let color_type = find_color(t);
    color_thief::get_palette(img, color_type, 10, 2)
}

fn find_color(t: image::ColorType) -> ColorFormat {
    match t {
        image::ColorType::Rgb8 => ColorFormat::Rgb,
        image::ColorType::Rgba8 => ColorFormat::Rgba,
        _ => unreachable!(),
    }
}

fn get_text_color(c: &rgb::RGB8) -> rgb::RGB8 {
    let mut color = rgb::RGB::new(255u8, 255u8, 255u8); //White color;

    let threshold = (0.2126 * c.r as f64) + (0.7152 * c.g as f64) + (0.0722 * c.b as f64);

    if threshold > 127.5 {
        color = rgb::RGB::new(0u8, 0u8, 0u8); //Black color;
    }

    color
}
